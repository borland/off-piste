FROM gnar/base:1.0

ENV PROJECT_NAME off-piste
ENV PYTHONPATH /code

WORKDIR /code/$PROJECT_NAME
RUN touch __init__.py

COPY app/requirements.txt app/requirements.txt
RUN yes | pip3 install --upgrade pip setuptools
RUN yes | pip3 install -r app/requirements.txt

COPY app app

EXPOSE 80

ENTRYPOINT ["python3"]

CMD ["app/main.py"]
